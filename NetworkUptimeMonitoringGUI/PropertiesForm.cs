﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkUptimeMonitoringDatabase.Database;
using System.ServiceProcess;

namespace NetworkUptimeMonitoringGUI
{
    public partial class PropertiesForm : Form
    {
        public PropertiesForm()
        {
            InitializeComponent();
            string ipAddress;
            int frequency;

            using (var context = new DatabaseContext())
            {
                ipAddress = context.Configs.Where(g => g.ParamName == "IPAddress").Select(g => g.ParamValue).FirstOrDefault();
                int.TryParse(context.Configs.Where(g => g.ParamName == "Frequency").Select(g => g.ParamValue).FirstOrDefault(), out frequency);
            }

            frequencyNumericField.Value = frequency;
            iPAddressTextField.Text = ipAddress;
       }

        private void okButton_Click(object sender, EventArgs e)
        {
            using (var context = new DatabaseContext())
            {
                NetworkUptimeMonitoringDatabase.Database.Tables.Configs newIpAddress = context.Configs.Where(g => g.ParamName == "IPAddress").FirstOrDefault();
                NetworkUptimeMonitoringDatabase.Database.Tables.Configs newFrequency = context.Configs.Where(g => g.ParamName == "Frequency").FirstOrDefault();

                newIpAddress.ParamValue = iPAddressTextField.Text;
                newFrequency.ParamValue = frequencyNumericField.Value.ToString();

                context.SaveChanges();
            }
            MessageBox.Show("To apply changes restart service", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        public static void RestartService()
        {
            ServiceController service = new ServiceController("NetworkUptimeMonitoring");
            try
            {
                service.Stop();
                service.Start();
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while checking Service, make sure that service is installed \n " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
