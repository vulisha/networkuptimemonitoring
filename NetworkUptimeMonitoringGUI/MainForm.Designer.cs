﻿namespace NetworkUptimeMonitoringGUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.propertiesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsDataGrid = new System.Windows.Forms.DataGridView();
            this.offlineTimeLabel = new System.Windows.Forms.Label();
            this.onlineTimeLabel = new System.Windows.Forms.Label();
            this.offlineTimeValue = new System.Windows.Forms.Label();
            this.onlineTimeValue = new System.Windows.Forms.Label();
            this.offlineButton = new System.Windows.Forms.Button();
            this.onlineButton = new System.Windows.Forms.Button();
            this.showAllButton = new System.Windows.Forms.Button();
            this.fromDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.toDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.showRangeDateTimePicker = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.stopServicebutton = new System.Windows.Forms.Button();
            this.startServiceButton1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.averagePingLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.resultsToDisplay = new System.Windows.Forms.NumericUpDown();
            this.daysOnLabel = new System.Windows.Forms.Label();
            this.daysOffLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsToDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.propertiesToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(779, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.exportToolStripMenuItem.Text = "Export";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(107, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // propertiesToolStripMenuItem
            // 
            this.propertiesToolStripMenuItem.Name = "propertiesToolStripMenuItem";
            this.propertiesToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.propertiesToolStripMenuItem.Text = "Properties";
            this.propertiesToolStripMenuItem.Click += new System.EventHandler(this.propertiesToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // reportsDataGrid
            // 
            this.reportsDataGrid.AllowUserToAddRows = false;
            this.reportsDataGrid.AllowUserToDeleteRows = false;
            this.reportsDataGrid.AllowUserToOrderColumns = true;
            this.reportsDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.reportsDataGrid.Location = new System.Drawing.Point(204, 27);
            this.reportsDataGrid.Name = "reportsDataGrid";
            this.reportsDataGrid.ReadOnly = true;
            this.reportsDataGrid.Size = new System.Drawing.Size(563, 344);
            this.reportsDataGrid.TabIndex = 2;
            // 
            // offlineTimeLabel
            // 
            this.offlineTimeLabel.AutoSize = true;
            this.offlineTimeLabel.Location = new System.Drawing.Point(12, 27);
            this.offlineTimeLabel.Name = "offlineTimeLabel";
            this.offlineTimeLabel.Size = new System.Drawing.Size(66, 13);
            this.offlineTimeLabel.TabIndex = 3;
            this.offlineTimeLabel.Text = "Offline Time:";
            // 
            // onlineTimeLabel
            // 
            this.onlineTimeLabel.AutoSize = true;
            this.onlineTimeLabel.Location = new System.Drawing.Point(12, 69);
            this.onlineTimeLabel.Name = "onlineTimeLabel";
            this.onlineTimeLabel.Size = new System.Drawing.Size(66, 13);
            this.onlineTimeLabel.TabIndex = 4;
            this.onlineTimeLabel.Text = "Online Time:";
            // 
            // offlineTimeValue
            // 
            this.offlineTimeValue.AutoSize = true;
            this.offlineTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.offlineTimeValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.offlineTimeValue.Location = new System.Drawing.Point(11, 41);
            this.offlineTimeValue.Name = "offlineTimeValue";
            this.offlineTimeValue.Size = new System.Drawing.Size(51, 20);
            this.offlineTimeValue.TabIndex = 5;
            this.offlineTimeValue.Text = "0:0:0";
            this.offlineTimeValue.Click += new System.EventHandler(this.label1_Click);
            // 
            // onlineTimeValue
            // 
            this.onlineTimeValue.AutoSize = true;
            this.onlineTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.onlineTimeValue.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.onlineTimeValue.Location = new System.Drawing.Point(11, 84);
            this.onlineTimeValue.Name = "onlineTimeValue";
            this.onlineTimeValue.Size = new System.Drawing.Size(51, 20);
            this.onlineTimeValue.TabIndex = 6;
            this.onlineTimeValue.Text = "0:0:0";
            // 
            // offlineButton
            // 
            this.offlineButton.Location = new System.Drawing.Point(125, 37);
            this.offlineButton.Name = "offlineButton";
            this.offlineButton.Size = new System.Drawing.Size(75, 23);
            this.offlineButton.TabIndex = 7;
            this.offlineButton.Text = "Offline";
            this.offlineButton.UseVisualStyleBackColor = true;
            this.offlineButton.Click += new System.EventHandler(this.offlineButton_Click);
            // 
            // onlineButton
            // 
            this.onlineButton.Location = new System.Drawing.Point(125, 66);
            this.onlineButton.Name = "onlineButton";
            this.onlineButton.Size = new System.Drawing.Size(75, 23);
            this.onlineButton.TabIndex = 8;
            this.onlineButton.Text = "Online";
            this.onlineButton.UseVisualStyleBackColor = true;
            this.onlineButton.Click += new System.EventHandler(this.onlineButton_Click);
            // 
            // showAllButton
            // 
            this.showAllButton.Location = new System.Drawing.Point(125, 94);
            this.showAllButton.Name = "showAllButton";
            this.showAllButton.Size = new System.Drawing.Size(75, 23);
            this.showAllButton.TabIndex = 9;
            this.showAllButton.Text = "Show All";
            this.showAllButton.UseVisualStyleBackColor = true;
            this.showAllButton.Click += new System.EventHandler(this.showAllButton_Click);
            // 
            // fromDateTimePicker
            // 
            this.fromDateTimePicker.Location = new System.Drawing.Point(3, 234);
            this.fromDateTimePicker.Name = "fromDateTimePicker";
            this.fromDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.fromDateTimePicker.TabIndex = 10;
            // 
            // toDateTimePicker
            // 
            this.toDateTimePicker.Location = new System.Drawing.Point(3, 280);
            this.toDateTimePicker.Name = "toDateTimePicker";
            this.toDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.toDateTimePicker.TabIndex = 11;
            // 
            // showRangeDateTimePicker
            // 
            this.showRangeDateTimePicker.Location = new System.Drawing.Point(111, 306);
            this.showRangeDateTimePicker.Name = "showRangeDateTimePicker";
            this.showRangeDateTimePicker.Size = new System.Drawing.Size(85, 23);
            this.showRangeDateTimePicker.TabIndex = 12;
            this.showRangeDateTimePicker.Text = "Show range";
            this.showRangeDateTimePicker.UseVisualStyleBackColor = true;
            this.showRangeDateTimePicker.Click += new System.EventHandler(this.showRangeDateTimePicker_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 215);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "From";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 264);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(20, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "To";
            // 
            // stopServicebutton
            // 
            this.stopServicebutton.Location = new System.Drawing.Point(111, 348);
            this.stopServicebutton.Name = "stopServicebutton";
            this.stopServicebutton.Size = new System.Drawing.Size(85, 23);
            this.stopServicebutton.TabIndex = 15;
            this.stopServicebutton.Text = "Stop Service";
            this.stopServicebutton.UseVisualStyleBackColor = true;
            this.stopServicebutton.Click += new System.EventHandler(this.stopServicebutton_Click);
            // 
            // startServiceButton1
            // 
            this.startServiceButton1.Location = new System.Drawing.Point(6, 348);
            this.startServiceButton1.Name = "startServiceButton1";
            this.startServiceButton1.Size = new System.Drawing.Size(85, 23);
            this.startServiceButton1.TabIndex = 16;
            this.startServiceButton1.Text = "Start Service";
            this.startServiceButton1.UseVisualStyleBackColor = true;
            this.startServiceButton1.Click += new System.EventHandler(this.startServiceButton1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Average Ping:";
            // 
            // averagePingLabel
            // 
            this.averagePingLabel.AutoSize = true;
            this.averagePingLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.averagePingLabel.Location = new System.Drawing.Point(15, 144);
            this.averagePingLabel.Name = "averagePingLabel";
            this.averagePingLabel.Size = new System.Drawing.Size(19, 20);
            this.averagePingLabel.TabIndex = 18;
            this.averagePingLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(108, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 13);
            this.label4.TabIndex = 19;
            this.label4.Text = "Number of results:";
            // 
            // resultsToDisplay
            // 
            this.resultsToDisplay.Location = new System.Drawing.Point(111, 144);
            this.resultsToDisplay.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.resultsToDisplay.Name = "resultsToDisplay";
            this.resultsToDisplay.Size = new System.Drawing.Size(87, 20);
            this.resultsToDisplay.TabIndex = 21;
            this.resultsToDisplay.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // daysOnLabel
            // 
            this.daysOnLabel.AutoSize = true;
            this.daysOnLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.daysOnLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.daysOnLabel.Location = new System.Drawing.Point(11, 188);
            this.daysOnLabel.Name = "daysOnLabel";
            this.daysOnLabel.Size = new System.Drawing.Size(19, 20);
            this.daysOnLabel.TabIndex = 25;
            this.daysOnLabel.Text = "0";
            // 
            // daysOffLabel
            // 
            this.daysOffLabel.AutoSize = true;
            this.daysOffLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.daysOffLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.daysOffLabel.Location = new System.Drawing.Point(107, 188);
            this.daysOffLabel.Name = "daysOffLabel";
            this.daysOffLabel.Size = new System.Drawing.Size(19, 20);
            this.daysOffLabel.TabIndex = 24;
            this.daysOffLabel.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 23;
            this.label7.Text = "Days on:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(108, 174);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Days off:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 383);
            this.Controls.Add(this.daysOnLabel);
            this.Controls.Add(this.daysOffLabel);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.resultsToDisplay);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.averagePingLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.startServiceButton1);
            this.Controls.Add(this.stopServicebutton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.showRangeDateTimePicker);
            this.Controls.Add(this.toDateTimePicker);
            this.Controls.Add(this.fromDateTimePicker);
            this.Controls.Add(this.showAllButton);
            this.Controls.Add(this.onlineButton);
            this.Controls.Add(this.offlineButton);
            this.Controls.Add(this.onlineTimeValue);
            this.Controls.Add(this.offlineTimeValue);
            this.Controls.Add(this.onlineTimeLabel);
            this.Controls.Add(this.offlineTimeLabel);
            this.Controls.Add(this.reportsDataGrid);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Network Uptime Monitor";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.reportsDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.resultsToDisplay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.DataGridView reportsDataGrid;
        private System.Windows.Forms.Label offlineTimeLabel;
        private System.Windows.Forms.Label onlineTimeLabel;
        private System.Windows.Forms.Label offlineTimeValue;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.Label onlineTimeValue;
        private System.Windows.Forms.Button offlineButton;
        private System.Windows.Forms.Button onlineButton;
        private System.Windows.Forms.Button showAllButton;
        private System.Windows.Forms.DateTimePicker fromDateTimePicker;
        private System.Windows.Forms.DateTimePicker toDateTimePicker;
        private System.Windows.Forms.Button showRangeDateTimePicker;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button stopServicebutton;
        private System.Windows.Forms.Button startServiceButton1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label averagePingLabel;
        private System.Windows.Forms.ToolStripMenuItem propertiesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown resultsToDisplay;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Label daysOnLabel;
        private System.Windows.Forms.Label daysOffLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
    }
}

