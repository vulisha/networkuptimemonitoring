﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkUptimeMonitoringDatabase.Database;
using System.ServiceProcess;
using System.Security.Principal;

namespace NetworkUptimeMonitoringGUI
{
    public partial class MainForm : Form
    {
        //List<NetworkUptimeMonitoringDatabase.Database.Tables.Reports> dataSet;
        public MainForm()
        {
            InitializeDatabase db = new InitializeDatabase();
            InitializeComponent();
            InitializeData();
            setAllformValues();

        }

        private void InitializeData()
        {
            
            fromDateTimePicker.Format = DateTimePickerFormat.Custom;
            fromDateTimePicker.CustomFormat = "     yyyy-MM-dd HH:mm:ss";
            toDateTimePicker.Format = DateTimePickerFormat.Custom;
            toDateTimePicker.CustomFormat = "     yyyy-MM-dd HH:mm:ss";

            if(ServiceStatus() == ServiceControllerStatus.Running)
            {
                startServiceButton1.Enabled = false;
            }
            else
            {
                stopServicebutton.Enabled = false;
            }
        }
        private void setAllformValues()
        {
            double offlineTime;
            double onlineTime;
            double averagePing;

            using (var context = new DatabaseContext())
            {

                reportsDataGrid.DataSource = context.Reports.OrderByDescending(g => g.ID).Take((int)resultsToDisplay.Value).ToList();
                offlineTime = context.Reports.Where(g => g.Status == false).Select(g=> g.Duration).DefaultIfEmpty(0).Sum();
                onlineTime = context.Reports.Where(g => g.Status == true).Select(g => g.Duration).DefaultIfEmpty(0).Sum();
                averagePing = context.Reports.Where(g => g.Status == true).Select(g => g.RoundTripTime).DefaultIfEmpty(0).Average();
            }
            setFormValues(offlineTime, onlineTime, averagePing);
        }
        private void setFormValues(double offlineTime, double onlineTime, double averagePing)
        {
            TimeSpan initial = TimeSpan.FromSeconds(0);
 
            offlineTimeValue.Text = TimeSpan.FromSeconds(offlineTime).ToString(@"dd\.hh\:mm\:ss");
            onlineTimeValue.Text = TimeSpan.FromSeconds(onlineTime).ToString(@"dd\.hh\:mm\:ss");
            averagePingLabel.Text = averagePing.ToString("F");

            daysOffLabel.Text = ConvertSecondsToTotalDays(offlineTime).ToString("F2");
            daysOnLabel.Text = ConvertSecondsToTotalDays(onlineTime).ToString("F2");

        }

        public static double ConvertSecondsToTotalDays(double seconds)
        {
            TimeSpan result = TimeSpan.FromSeconds(seconds);

            return result.TotalDays;
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void offlineButton_Click(object sender, EventArgs e)
        {
            using (var context = new DatabaseContext())
            {
                reportsDataGrid.DataSource = context.Reports.Where(g=>g.Status == false).OrderByDescending(g => g.ID).Take((int)resultsToDisplay.Value).ToList();
            }
        }

        private void onlineButton_Click(object sender, EventArgs e)
        {
            using (var context = new DatabaseContext())
            {
                reportsDataGrid.DataSource = context.Reports.Where(g => g.Status == true).OrderByDescending(g => g.ID).Take((int)resultsToDisplay.Value).ToList();
            }
        }

        private void showAllButton_Click(object sender, EventArgs e)
        {
            setAllformValues();
        }

        private void showRangeDateTimePicker_Click(object sender, EventArgs e)
        {
            using (var context = new DatabaseContext())
            {
                double offlineTime;
                double onlineTime;
                double averagePing;

                reportsDataGrid.DataSource = context.Reports.Where(g => g.RequestTime >= fromDateTimePicker.Value && g.RequestTime <= toDateTimePicker.Value).OrderByDescending(g => g.ID).Take((int)resultsToDisplay.Value).ToList();
                offlineTime = context.Reports.Where(g => g.Status == false && g.RequestTime >= fromDateTimePicker.Value && g.RequestTime <= toDateTimePicker.Value).Select(g => g.Duration).DefaultIfEmpty(0).Sum();
                onlineTime = context.Reports.Where(g => g.Status == true && g.RequestTime >= fromDateTimePicker.Value && g.RequestTime <= toDateTimePicker.Value).Select(g => g.Duration).DefaultIfEmpty(0).Sum();
                averagePing = context.Reports.Where(g => g.Status == true && g.RequestTime >= fromDateTimePicker.Value && g.RequestTime <= toDateTimePicker.Value).Select(g => g.RoundTripTime).DefaultIfEmpty(0).Average();
                setFormValues(offlineTime, onlineTime, averagePing);
            }
        }

        public static bool StartService()
        {
            ServiceController service = new ServiceController("NetworkUptimeMonitoring");
            try
            {
                service.Start();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while checking Service, make sure that service is installed \n " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public static bool StopService()
        {
            ServiceController service = new ServiceController("NetworkUptimeMonitoring");
            try
            {
                service.Stop();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while checking Service, make sure that service is installed \n " + ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public ServiceControllerStatus ServiceStatus()
        {
            ServiceController service = new ServiceController("NetworkUptimeMonitoring");
            try
            {
                return service.Status;
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured while checking Service, make sure that service is installed \n " + ex.ToString() , "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return ServiceControllerStatus.Stopped;
            }
        }

        private void stopServicebutton_Click(object sender, EventArgs e)
        {
            if(!IsAdministrator())
            {
                MessageBox.Show("You need to run this aplication as administrator to start or stop service. \n Close the app, right click icon and select \"Run as Administrator\" ", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if(ServiceStatus() != ServiceControllerStatus.Running)
                {
                    stopServicebutton.Enabled = true;
                    startServiceButton1.Enabled = true;
                    return;
                }

                if (StopService())
                {
                    startServiceButton1.Enabled = true;
                    stopServicebutton.Enabled = false;
                }
            }

        }

        private void startServiceButton1_Click(object sender, EventArgs e)
        {
            if (!IsAdministrator())
            {
                MessageBox.Show("You need to run this aplication as administrator to start or stop service. \n Close the app, right click icon and select \"Run as Administrator\" ", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (ServiceStatus() != ServiceControllerStatus.Stopped)
                {
                    stopServicebutton.Enabled = true;
                    startServiceButton1.Enabled = true;
                    return;
                }
                if (StartService())
                {
                    stopServicebutton.Enabled = true;
                    startServiceButton1.Enabled = false;
                }
            }
        }

        public static bool IsAdministrator()
        {
            return (new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                      .IsInRole(WindowsBuiltInRole.Administrator);
        }

        private void propertiesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var form = new PropertiesForm();
            form.Show();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutWindow aboutWindow = new AboutWindow();
            aboutWindow.Show();
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteRecords deleteRecords = new DeleteRecords();
            deleteRecords.Show();
        }
    }
}
