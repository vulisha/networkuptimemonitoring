﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NetworkUptimeMonitoringDatabase.Database;

namespace NetworkUptimeMonitoringGUI
{
    public partial class DeleteRecords : Form
    {
        public DeleteRecords()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Are you sure you want to delete all records under ID " + recordIdToDelete.Value.ToString(), "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if(result == DialogResult.OK)
            {
                var db = new DatabaseHelper();
                db.BackupDatabase();

                using (var context = new DatabaseContext())
                {
                    var items = context.Reports.Where(g => g.ID < recordIdToDelete.Value);

                    context.Reports.RemoveRange(items);
                    context.SaveChangesAsync();

                }
                this.Close();
            }
        }
    }
}
