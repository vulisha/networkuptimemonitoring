﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Net.NetworkInformation;
using NetworkUptimeMonitoringDatabase.Database;
using NetworkUptimeMonitoringDatabase.Database.Tables;

namespace NetworkUptimeMonitoring
{
    public partial class MonitorService : ServiceBase
    {
        Timer timer = new Timer();
        DateTime previous = DateTime.Now;
        internal void TestStartupAndStop()
        {
            this.OnStart(null);
            Console.ReadLine();
            while(true)
            {
                Console.ReadLine();
            }
            this.OnStop();
        }

        public MonitorService()
        {
            InitializeComponent();
        }
        protected override void OnStart(string[] args)
        {
            InitializeDatabase db = new InitializeDatabase();

            try
            {
                WriteToFile("Service is started at " + DateTime.Now);
                timer.Elapsed += new ElapsedEventHandler(OnElapsedTime);
                using (var context = new DatabaseContext())
                {
                    double result;
                    double.TryParse(context.Configs.Where(g => g.ParamName == "Frequency").Select(g => g.ParamValue).FirstOrDefault(), out result);
                    timer.Interval = result;
                }
                timer.Enabled = true;
            }
            catch (Exception ex)
            {
                WriteToFile("Service failed to start at" + DateTime.Now + '\n' + ex);
                throw;
            }

        }
        protected override void OnStop()
        {
            WriteToFile("Service is stopped at " + DateTime.Now);
        }
        private void OnElapsedTime(object source, ElapsedEventArgs e)
        {
            try
            {
                using (var context = new DatabaseContext())
                {
                    string ipAddress = context.Configs.Where(g => g.ParamName == "IPAddress").Select(g => g.ParamValue).FirstOrDefault();
                    Ping ping = new Ping();
                    PingReply pingReplay = ping.Send(ipAddress);

                    Reports result = new Reports();
                    result.IPAddress = ipAddress;
                    result.RequestTime = DateTime.Now;
                    result.Status = (pingReplay.Status == IPStatus.Success) ? true : false;
                    result.RoundTripTime = pingReplay.RoundtripTime;
                    result.Duration = timer.Interval/1000;

                    context.Reports.Add(result);
                    context.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                WriteToFile("Service failed to write at" + DateTime.Now + '\n' + ex);
                throw;
            }

        }
        public void WriteToFile(string Message)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "\\Logs";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\ServiceLog_" + DateTime.Now.Date.ToShortDateString().Replace('/', '_') + ".txt";
            if (!File.Exists(filepath))
            { 
                using (StreamWriter sw = File.CreateText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
            else {
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    sw.WriteLine(Message);
                }
            }
        }
    }
}
