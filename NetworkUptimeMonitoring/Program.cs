﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace NetworkUptimeMonitoring
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //if (Environment.UserInteractive)
            //{
            //    MonitorService service1 = new MonitorService();
            //    service1.TestStartupAndStop();
            //}
            //else
            //{
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new MonitorService()
                };
                ServiceBase.Run(ServicesToRun);
            //}
        }
    }
}
