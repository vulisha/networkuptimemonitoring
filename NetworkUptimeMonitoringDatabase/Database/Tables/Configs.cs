﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Linq.Mapping;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NetworkUptimeMonitoringDatabase.Database.Tables
{
    [Table(Name = "Configs")]
    public class Configs
    {
        [Column(Name = "ID", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int ID { get; set; }

        [Column(Name = "ParamName", DbType = "VARCHAR")]
        public string ParamName { get; set; }

        [Column(Name = "ParamValue", DbType = "VARCHAR")]
        public string ParamValue { get; set; }

    }
}
