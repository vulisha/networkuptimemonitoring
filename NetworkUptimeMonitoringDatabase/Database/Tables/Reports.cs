﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Linq.Mapping;
using System.Data.SQLite;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkUptimeMonitoringDatabase.Database.Tables
{
    [Table(Name = "Reports")]
    public class Reports
    {
        [Column(Name = "ID", IsDbGenerated = true, IsPrimaryKey = true, DbType = "INTEGER")]
        [Key]
        public int ID { get; set; }

        [Column(Name = "RequestTime", DbType = "DATETIME")]
        public DateTime RequestTime { get; set; }

        [Column(Name = "IPAddress", DbType = "VARCHAR")]
        public string IPAddress { get; set; }

        [Column(Name = "Status", DbType = "BIT")]
        public bool Status { get; set; }

        [Column(Name = "RoundTripTime", DbType = "INTIGER")]
        public long RoundTripTime { get; set; }

        [Column(Name = "Duration", DbType = "INTIGER")]
        public double Duration { get; set; }
    }
}
