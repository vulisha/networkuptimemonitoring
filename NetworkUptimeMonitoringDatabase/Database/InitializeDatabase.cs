﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NetworkUptimeMonitoringDatabase.Database
{
    public class InitializeDatabase
    {

        private string DatabaseLocation;
        public InitializeDatabase()
        {
            this.DatabaseLocation = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"];
#if DEBUG
            this.DatabaseLocation = "C:\\Users\\Vule\\Documents\\Visual Studio 2015\\Projects\\NetworkUptimeMonitoring\\NetworkUptimeMonitoring\\bin\\Debug\\" + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"];
#endif

            Initialize();
        }

        void Initialize()
        {
            if (!System.IO.File.Exists(DatabaseLocation))
            {
                createNewDatabase();
                createTablesAndInitializeConfigs();
            }
            else
            {
                //for database update
            }
        }
        void createNewDatabase()
        {
            SQLiteConnection.CreateFile(DatabaseLocation);
        }

        SQLiteConnection connectToDatabase(string databaseLocation)
        {
            SQLiteConnection m_dbConnection = new SQLiteConnection("Data Source=" + databaseLocation + ";Version=3;");
            m_dbConnection.Open();
            return m_dbConnection;
        }
        void createTablesAndInitializeConfigs()
        {
            SQLiteConnection m_dbConnection = connectToDatabase(DatabaseLocation);

            string sql = @"CREATE TABLE Reports (
                                    ID INTEGER PRIMARY KEY AUTOINCREMENT,
                                    RequestTime DATETIME,
                                    IPAddress VARCHAR(18),
                                    Status BIT,
                                    RoundTripTime INTEGER,
                                    Duration REAL
                                );";
            SQLiteCommand command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = @"CREATE TABLE Configs (
                                    ID INTEGER PRIMARY KEY AUTOINCREMENT,
                                    ParamName VARCHAR(20),
                                    ParamValue VARCHAR(20)
                                );";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "INSERT INTO Configs (ParamName, ParamValue) values ('Frequency', '5000')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();
            sql = "INSERT INTO Configs (ParamName, ParamValue) values ('IPAddress', '8.8.8.8')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

            sql = "INSERT INTO Configs (ParamName, ParamValue) values ('Version','" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString() + "')";
            command = new SQLiteCommand(sql, m_dbConnection);
            command.ExecuteNonQuery();

        }

    }
}
