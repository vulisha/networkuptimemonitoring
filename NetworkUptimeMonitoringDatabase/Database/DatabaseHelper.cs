﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetworkUptimeMonitoringDatabase.Database
{
    public class DatabaseHelper
    {
        private string DatabaseLocation;
        public DatabaseHelper()
        {
            this.DatabaseLocation = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"];
#if DEBUG
            this.DatabaseLocation = "C:\\Users\\Vule\\Documents\\Visual Studio 2015\\Projects\\NetworkUptimeMonitoring\\NetworkUptimeMonitoring\\bin\\Debug\\" + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"];
#endif
        }

        public void BackupDatabase()
        {
            try
            {
                System.IO.File.Copy(DatabaseLocation, DatabaseLocation + ".backup", true);
            }
            catch { }
        }
    }
}
