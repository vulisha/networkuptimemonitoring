﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NetworkUptimeMonitoringDatabase.Database.Tables;

namespace NetworkUptimeMonitoringDatabase.Database
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() :
            base(new SQLiteConnection()
            {

#if DEBUG
                ConnectionString = new SQLiteConnectionStringBuilder() { DataSource = "C:\\Users\\Vule\\Documents\\Visual Studio 2015\\Projects\\NetworkUptimeMonitoring\\NetworkUptimeMonitoring\\bin\\Debug\\" + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"], ForeignKeys = true }.ConnectionString
#else
                ConnectionString = new SQLiteConnectionStringBuilder() { DataSource = AppDomain.CurrentDomain.BaseDirectory + System.Configuration.ConfigurationManager.AppSettings["DatabaseLocation"] , ForeignKeys = true }.ConnectionString
#endif
            }, true)
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Reports> Reports { get; set; }
        public DbSet<Configs> Configs { get; set; }
    }
}
